<?php

/**
 * @file
 * 	Detect Closure library on system
 */

/**
 * Detect Google Library on system
 */
function closure_detect_verion() {
  // @TODO: Check closure compile file
  if (file_exists(closure_get_base_file())) {
    return array(
      TRUE,
      '1.0', // @TODO: check Closure version
    );
  }
  else {
    return array(
      FALSE,
      libraries_get_path('closure-library'),
    );
  }
}

/**
 * Helper function to get base file of Closure Library
 */
function closure_get_base_file() {
  return libraries_get_path('closure-library') . '/closure/goog/base.js';
}